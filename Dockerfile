FROM openjdk:17-jdk as bootJar

COPY . /project

RUN cd /project && ./gradlew bootJar

FROM openjdk:17

COPY --from=bootJar /project/build/libs/*.jar /project.jar

CMD java -jar /project.jar
